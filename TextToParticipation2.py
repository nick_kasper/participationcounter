import numpy as np
import pandas as pd
import os 
import argparse
from canvasapi import Canvas
import string
import nltk
import matplotlib.pyplot as plt
import seaborn as sns
from fuzzywuzzy import fuzz
from fuzzywuzzy import process

nltk.download('stopwords')


def list_files(dir):
    file_list = []
    for root, dirs, files in os.walk(dir):
        for name in files:
            file_list.append(os.path.join(root,name))
    return file_list
    
def extract_paths(file_list):
    text_paths = []
    
    for element in file_list:
        if '.txt' in element:
            text_paths.append(element)
        else:
            pass
    return text_paths

def grab_students(base_url, access_token, course_number):

    #Intializing canvas object
    
    canvas = Canvas(base_url, access_token)
    
    #initialize course

    course = canvas.get_course(course_number)

    #grab students
    
    users = course.get_users(enrollment_type=['student'])

    #create and fill list with students

    students = []
    for user in users:
        students.append(str(user).split('(')[0])

    student_dataframe = pd.DataFrame(students, columns = ['Name'])

    return student_dataframe


    
def grab_chat(text_paths):
    #for each text file, open corresponding text file
    #for each element in the open text file, count the number of times each person said something 
    text_files = []
    text_lines = []
    name = ''
    names = []
    message = ''
    messages = []

    for element in text_paths:
        text_files= open(element, errors ='ignore')
        text_lines = text_files.readlines()
        for line in text_lines:
            if 'Privately' in line:
                pass
            elif 'Dr.' in line:
                pass
            elif not line.strip():
                pass
            elif 'Kasper' in line:
                pass
            elif 'From' not in line:
                pass
            elif ':' not in line:
                pass
            elif len(line) > 200:
                pass
            else:
                name = line.split('From')[1].split(':')[0]
                names.append(name)
                message = line.split(':')[3]
                messages.append(message)
                

    zippedList = list(zip(names,messages))
    init_chat_dataframe = pd.DataFrame(zippedList, columns = ['Name', 'Message'])

    return init_chat_dataframe


def string_similarity(str1, str2):
    Ratio = fuzz.ratio(str1.lower(), str2.lower())
    Partial_Ratio = fuzz.partial_ratio(str1.lower(), str2.lower())
    Token_Sort_Ratio = fuzz.token_sort_ratio(str1,str2)
    Token_Set_Ratio = fuzz.token_set_ratio(str1,str2)

    print(Ratio)
    print(Partial_Ratio)
    print(Token_Sort_Ratio)
    print(Token_Set_Ratio)

def highest_string_similarity(str2Match, strOptions):
    '''
    Returns string with highest similarity to str2Match out of strOptions

    Parameters:
    str2Match(str): String you're comparing all the others too
    strOptions(list): List of strings 
    '''
    highest = process.extractOne(str2Match, strOptions)
    

    return highest



def naive_tally(chat_dataframe, students):

    '''
    Creates a vector of tallys for each student, where a tally is earned by speaking in chat
    
    Parameters: 
    
    chat_dataframe (df): Dataframe of messages and names for each message
    students(list): List of students in class generated by Canvas API in grab_students

    '''
    matched_names = []

    for name in chat_dataframe['Name']:
        match = pd.Series(highest_string_similarity(str(name), list(students['Name'])))[0]
        matched_names.append(match)

    chat_dataframe['matched_name'] = matched_names

    chat_dataframe.to_csv('Matched_Names.csv')
    
    print(chat_dataframe)
    
    
def main():
    
    file_list = list_files('C:/Users/nickk/Documents/Zoom')

    #grabbing keys
    keys = open('keys.txt')
    lines = keys.readlines()
    base_url = lines[0].split("'")[1].split("'")[0]
    access_token = lines[1].split("'")[1].split("'")[0]

    students = grab_students(base_url = str(base_url), access_token = str(access_token), course_number = 1280443)
    paths = extract_paths(file_list)

    chat_dataframe = grab_chat(paths)

    naive_tally(chat_dataframe, students)

  

if __name__ == '__main__':
    main() 