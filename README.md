### ParticipationCounter
    The purpose of this project is to compute class participation scores for each student in a class. 

    In the Naive Case, we would assign a participation point for each time a student speaks. We could then collect the participation points
    and append them each time a new Zoom text log becomes available.

    If we wanted to more robustly define what merits a point, we could train a machine learning model on spam/not spam text datasets, and then
    predict if each message is deemed a point (not spam). We could then collect participation points by classifying chat log messages and keep counts 
    for each member.

    This version uses Naive Bayes Classifier. 

    


