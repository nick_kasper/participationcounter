### The Levenshtein Distance
    - Measures how apart are two sequences of words
    - Measures the minimum number of edits that you need to change a one-word sequence into the other

### Fuzzy Wuzzy
    -If the short string has length k and other length m, then the algo seeks the score of the best matching length -k substring