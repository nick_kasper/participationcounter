import numpy as np
import pandas as pd
import os 
import argparse
from canvasapi import Canvas
from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import MultinomialNB
from sklearn.preprocessing import LabelEncoder
from keras.models import Model
from keras.layers import LSTM, Activation, Dense, Dropout, Input, Embedding
from keras.optimizers import RMSprop
from keras.preproccessing.text import Tokenizer
from keras.preproccessing import sequence
from keras.utils import to_categorical
from keras.callbacks import EarlyStopping
import string
import nltk
from nltk.corpus import stopwords
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from sklearn import metrics
import matplotlib.pyplot as plt
import seasborn as sns


nltk.download('stopwords')


def list_files(dir):
    file_list = []
    for root, dirs, files in os.walk(dir):
        for name in files:
            file_list.append(os.path.join(root,name))
    return file_list
    
def extract_paths(file_list):
    text_paths = []
    
    for element in file_list:
        if '.txt' in element:
            text_paths.append(element)
        else:
            pass
    return text_paths

def grab_students(base_url, access_token, course_number):

    #Intializing canvas object
    
    canvas = Canvas(base_url, access_token)
    
    #initialize course

    course = canvas.get_course(course_number)

    #grab students
    
    users = course.get_users(enrollment_type=['student'])

    #create and fill list with students

    students = []
    for user in users:
        students.append(user)

    return students
    
    

def grab_chat(text_paths):
    #for each text file, open corresponding text file
    #for each element in the open text file, count the number of times each person said something 
    text_files = []
    text_lines = []
    name = ''
    names = []
    message = ''
    messages = []

    for element in text_paths:
        text_files= open(element, errors ='ignore')
        text_lines = text_files.readlines()
        for line in text_lines:
            if 'Privately' in line:
                pass
            elif 'Dr.' in line:
                pass
            elif not line.strip():
                pass
            elif 'From' not in line:
                pass
            elif ':' not in line:
                pass
            elif len(line) > 200:
                pass
            else:
                name = line.split('From')[1].split(':')[0]
                names.append(name)
                message = line.split(':')[3]
                messages.append(message)
    return names, messages



def convert_training_data():
    # import training dataset
    feature_dataset = pd.read_csv('spam.csv', delimiter = ',', encoding ='latin-1')
    feature_dataset.dropna(how='any',inplace=True, axis=1)
    feature_dataset.columns = ['label','message']
    
    # covert label to a numerical variable

    feature_dataset['num_label'] = feature_dataset.label.map({'ham':0, 'spam':1})
    feature_dataset['message_len'] = feature_dataset.apply(len)
    return feature_dataset


def text_process(data):
    '''
    pre procceses text data for ml
    Takes in a string of text, then:
    1) removes all punctuation
    2) removes all stopwords
    3) returns a list of the cleaned text

    parameters:
    data(dataframe): text dataframe

    '''
    stop_words = stopwords.words('english')
    
    # Checks charecters to see if they are in punctuation

    nopunc = [char for char in data if char not in string.punctuation]

    # Joins chars again to form string
    nopunc = ''.join(nopunc)

    # Remove stopwords

    return ' '.join([word for word in nopunc.split() if word.lower() not in stop_words])

def tokenize_and_model(data, model_type):

    #tokenize data

    data['clean_msg'] = data.message.apply(text_process)
    X = data.clean_msg
    Y = data.label
    le = LabelEncoder()
    Y = le.fit_transform(Y)
    Y = Y.reshape(-1,1)

    X_train, X_test, y_train, y_test = train_test_split(X,Y, test_size = 0.15)

    max_words = 1000
    max_len = 150
    tok = Tokenizer(num_words =max_words)
    tok.fit_on_texts(X_train)
    sequences = tok.texts_to_sequences(X_train)
    sequences_matrix = sequence.pad_sequences(sequences, maxlen= max_len)


    return max_len, sequences_matrix, y_train, X_test, y_test

    print(X.shape)
    print(y.shape)  

def RNN(max_len):
    inputs = Input(name= 'inputs', shape = [max_len])
    layer = Embedding(max_words,50,input_length=max_len)(inputs)
    layer = LSTM(64)(layer)
    layer = Dense(256,name='FC1')(layer)
    layer = Activation('relu')(layer)
    layer = Dropout(0.5)(layer)
    layer = Dense(1,name='out_layer')(layer)
    layer = Activation('sigmoid')(layer)
    model = Model(inputs=inputs,outputs=layer)

    return model

def compile_and_fit(model, sequences_matrix, Y_train, X_test, Y_test):
    model.summary()
    model.compile(loss = 'binary_crossentropy', optimizer = RMSprop(), metrics = ['accuracy'])

    # fit on the training data

    model.fit(sequences_matrix,Y_train,batch_size=128,epochs=10, validation_split=0.2
    ,callbacks=[EarlyStopping(monitor='val_loss',min_delta=0.0001)])

    #process the test set data

    test_sequences = tok.texts_to_sequences(X_test)
    test_sequences_matrix = sequence.pad_sequences(test_sequences,maxlen=max_len)

    accr = model.evaluate(test_sequences_matrix,Y_test)
    
    print('Test set\n  Loss: {:0.3f}\n  Accuracy: {:0.3f}'.format(accr[0],accr[1]))

    
    '''

    if model_type == 'NB':
        # Split into training and test set

        X_train, X_test, y_train, y_test = train_test_split(X,Y, test_size = 0.15)

    
        #instantiate the vectorizer
        vect = CountVectorizer()

        vect.fit(X_train)

        #learn from training data, then use it to create a document-term matrix
        X_train_dtm = vect.transform(X_train)

        #transform testing data (using fitted vocabulary) into a document-term matrix
        X_test_Dtm = vect.transform(X_test)

        #fits transformer to training data

        tfidf_transformer = TfidfTransformer()
        tfidf_transformer.fit(X_train_dtm)
        tfidf_transformer.transform(X_train_dtm)
        #instatiate multinomial naive bayes model

        classifier = MultinomialNB()

        # Fit model

        classifier.fit(X_train_dtm, y_train)

        # make class predictions for X_test_dtm
        y_pred_class = classifier.predict(X_test_Dtm)

        #calculate the accuracy of the class predictions

        class_accuracy = metrics.accuracy_score(y_test,y_pred_class)
        print("The class prediction accuracy score is is %s" %(class_accuracy))


        # create a confusion matrix

        class_confusion = metrics.confusion_matrix(y_test, y_pred_class)
        print("The class prediction confusion matrix is %s" %(class_confusion))

        #finds number of false positives and false negatives

        fp = len(X_test[y_pred_class > y_test])
        fn = len(X_test[y_pred_class < y_test])

        print("The number of false positives is %d and the number of false negatives is %d" %(fp,fn))

        #calculate predicted probabilites for X_test_Dtm
        y_pred_prob = classifier.predict_proba(X_test_Dtm)[:,1]
        print("The predicted probabilities are %s" %(y_pred_prob))
    

        #calculate AUC
        x = metrics.roc_auc_score(y_test, y_pred_prob)
        print("The AUC is %d" %(x))

        #creates pipeline and fit
        pipe = Pipeline([('bow', vect), ('tfid', tfidf_transformer), ('model', classifier)])
        pipe.fit(X_train, y_train)

        y_pred = pipe.predict(X_test)

        final_accuracy = metrics.accuracy_score(y_test, y_pred)
        print("The final accuracy score is %s" %(final_accuracy))

        final_confusion = metrics.confusion_matrix(y_test,y_pred)
        print("The final confusion matrix is %s" %(final_confusion))
        
        return pipe, vect, tfidf_transformer
    
    if model_type == 'LOGREG':
        # Split into training and test set

        X_train, X_test, y_train, y_test = train_test_split(X,y, test_size = 0.2, random_state = 0)

    
        #instantiate the vectorizer
        vect = CountVectorizer()

        vect.fit(X_train)

        #learn from training data, then use it to create a document-term matrix
        X_train_dtm = vect.transform(X_train)

        #transform testing data (using fitted vocabulary) into a document-term matrix
        X_test_dtm = vect.transform(X_test)

        #fits transformer to training data

        tfidf_transformer = TfidfTransformer()
        tfidf_transformer.fit(X_train_dtm)
        tfidf_transformer.transform(X_train_dtm)
        
        #instantiate logreg object

        classifier = LogisticRegression(solver ='liblinear')
        classifier.fit(X_train_dtm, y_train)

        y_pred_class = classifier.predict(X_test_dtm)
        
        y_pred_prob = classifier.predict_proba(X_test_dtm)[:,1]
        print("The predicted probabilities are %s" %(y_pred_prob))

        class_accuracy = metrics.accuracy_score(y_test, y_pred_class)

        print("The class prediction accuracy score is is %s" %(class_accuracy))

        confusion_matrix = metrics.confusion_matrix(y_test, y_pred_class)

        print("The class confusion matrix is %s" %(confusion_matrix))

        fp = len(X_test[y_pred_class > y_test])
        fn = len(X_test[y_pred_class < y_test])

        print("The number of false positives is %d and the number of false negatives is %d" %(fp,fn))

        pipe = Pipeline([('bow', vect), ('tfid', tfidf_transformer), ('model', classifier)])
        pipe.fit(X_train, y_train)

        y_pred = pipe.predict(X_test)

        final_accuracy = metrics.accuracy_score(y_test, y_pred)
        print("The final accuracy score is %s" %(final_accuracy))

        final_confusion = metrics.confusion_matrix(y_test,y_pred)
        print("The final confusion matrix is %s" %(final_confusion))
        
        return pipe, vect, tfidf_transformer

    '''

def determine_spam(pipe, vect, tfidf):
    #Determine if text log messages are spam or not spam 

    names, messages = grab_chat(text_paths = extract_paths(file_list= list_files(dir= 'C:/Users/nickk/Documents/Zoom')))
    
    total_dataframe = pd.DataFrame(data = (messages), columns = ['Messages'])

    total_dataframe['clean_msg'] = total_dataframe.Messages.apply(text_process)


    #transform testing data (using fitted vocabulary) into a document-term matrix
    #X_test_Dtm = vect.transform(total_dataframe['clean_msg'])


    total_dataframe['spam_or_ham'] = pipe.predict(total_dataframe['clean_msg'])


    
    print(total_dataframe[total_dataframe.spam_or_ham == 0].describe()) #ham
    print(total_dataframe[total_dataframe.spam_or_ham == 1].describe()) #spam

    total_dataframe.to_csv('outputlogreg.csv') 
    
    


def populate_dataframe():
    pass

def main():
    
    '''
    file_list = list_files('C:/Users/nickk/Documents/Zoom')

    paths = extract_paths(file_list)

    grab_chat(paths)
    
    keys = open('keys.txt')
    lines = keys.readlines()
    base_url = lines[0].split("'")[1].split("'")[0]
    access_token = lines[1].split("'")[1].split("'")[0]


    grab_students(base_url = str(base_url), access_token = str(access_token), course_number = 1289599)
    '''
    data = convert_training_data()
    max_len, sequence_matrix, y_train, X_test, Y_test= tokenize_and_model(data, 'NB')
    model = RNN(max_len)
    compile_and_fit(model, sequence_matrix, y_train, X_test, Y_test)
    #determine_spam(model, vect, tfidf)


if __name__ == '__main__':
    main() 